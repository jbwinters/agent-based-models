from collections import namedtuple

import click
import numpy as np
import scipy.spatial.distance as distance_metrics

WORLD_DIMS = 2

Message = namedtuple('Message', ['origin', 'name', 'value', 'strength'])

class Agent(object):
    def __init__(self):
        self.pdist = np.random.rand(1, 3)[0]
        self.loc = np.random.rand(1, WORLD_DIMS)[0] * 20 - 10
        self.neighbors = []

    def next(self):
        self.pdist = np.mean(self.neighbors, axis=0)
        self.neighbors = []

    @property
    def color(self):
        return np.around(self.pdist * 255)


def nearest_neighbors(a, agents, k=5):
    return [n for n in sorted(agents, 
        key=lambda x: distance_metrics.euclidean(a.loc, x.loc), 
        reverse=True)][:k]


def print_agents(agents, neighbors):
    for i, a in enumerate(agents):
        print i, a.color, np.mean([b.color for b in neighbors[a]], axis=0)


@click.command()
@click.option('-n', default=25, help='Number of agents')
@click.option('--iterations', '-i', default=100, help='Number of agents')
@click.option('-k', default=5, help='Number of neighbors to reflect')
def run(n=25, iterations=100, k=5):
    agents = [Agent() for i in range(n)]
    neighbors = {a: nearest_neighbors(a, agents, k=k) for a in agents}

    print '===BEGIN==='
    print_agents(agents, neighbors)

    for i in range(iterations):
        for a in agents:
            a.neighbors = [b.pdist for b in neighbors[a]]
        for a in agents:
            a.next()

    print '===END ({i})==='.format(i=i + 1)
    print_agents(agents, neighbors)

if __name__ == '__main__':
    run()
