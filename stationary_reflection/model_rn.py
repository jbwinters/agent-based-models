from collections import namedtuple

import click
import numpy as np
import scipy.spatial.distance as distance_metrics

WORLD_DIMS = 2

Message = namedtuple('Message', ['origin', 'name', 'value', 'strength'])

class Agent(object):
    def __init__(self):
        self.pdist = np.random.rand(1, 3)[0]
        self.loc = np.random.rand(1, WORLD_DIMS)[0] * 20 - 10
        self.neighbors = []

    def next(self):
        self.pdist = np.mean(self.neighbors, axis=0)
        self.neighbors = []

    @property
    def color(self):
        return np.around(self.pdist * 255)


def radius_neighbors(a, agents, r=2):
    return [n for n in agents if distance_metrics.euclidean(a.loc, n.loc) < r]


def print_agents(agents, neighbors):
    for i, a in enumerate(agents):
        print i, a.color, np.mean([b.color for b in neighbors[a]], axis=0), \
                a.loc, np.mean([b.loc for b in neighbors[a]], axis=0)


@click.command()
@click.option('-n', default=25, help='Number of agents')
@click.option('--iterations', '-i', default=100, help='Number of agents')
@click.option('--radius', '-r', default=2, help='Neighbor radius')
def run(n=25, iterations=100, radius=5):
    agents = [Agent() for i in range(n)]
    neighbors = {a: radius_neighbors(a, agents, r=radius) for a in agents}

    print '===BEGIN==='
    print_agents(agents, neighbors)

    for i in range(iterations):
        for a in agents:
            a.neighbors = [b.pdist for b in neighbors[a]]
        for a in agents:
            a.next()

    print '===END ({i})==='.format(i=i + 1)
    print_agents(agents, neighbors)

if __name__ == '__main__':
    run()
