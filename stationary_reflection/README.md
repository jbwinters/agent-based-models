Agents which grow to reflect the agents around them. Based on the idea that a human unconsciously learns to mimic/reflect the individuals near them. Agents are stationary for simplicity.

### `model_knn.py`
Every agent has a location and a color, and on every iteration the agent becomes the average color of its `k` nearest neighbors. Over time they all become the same color. Good model of adaptation to the internet.

### `model_rn.py`
Every agent has a location and a color, and on every iteration the agent becomes the average color of its neighbors within a given radius `r`. Over time they cluster themselves by location and color. Better model of real-life human interaction.
