Exploration of agents which are drawn to ideologically similar agents. 
Every agent has a random signal in its head and on every iteration, it emits that signal to all other agents. 
Each agent calculates a movement vector based on the aggregation of all signals it hears and the origin points of each signal.
Intention is to create a self-stabilizing model where units organize themselves according to an inner "truth".
If error is the difference of the world map from optimal, then expectation is that error decreases over time (but haven't tested).

Only a sketch - not fully working at the moment.
