from collections import namedtuple
import numpy as np
import scipy.spatial.distance as distance_metrics

WORLD_DIMS = 2

Message = namedtuple('Message', ['origin', 'name', 'value', 'strength'])

class Agent(object):
    def __init__(self):
        self.pdist = np.random.rand(1, 10)[0]
        self.loc = np.random.randint(-10, 11, size=WORLD_DIMS)
        self.messages = []


    def move(self):
        direction = [0] * WORLD_DIMS
        for msg in self.messages:
            direction += msg.origin * msg.strength
        direction = np.array([1 if d > 1 else -1 if d < -1 else int(d) for d in direction])
        self.loc += direction
        self.loc = np.array([-10 if d > 10
                else 10 if d < -10
                else int(d) for d in self.loc])
        self.messages = []

    def signal_strength(self, msg):
        return 1 / (1 + distance_metrics.euclidean(msg.value, self.pdist))

    def accept(self, msg):
        if msg.name == 'signal':
            strength = self.signal_strength(msg)
            self.messages += [Message(msg.origin, msg.name, msg.value, strength)]


def print_map(agents, label):
    print '{}{}{}'.format('=' * 5, label, '=' * 5)
    m = []
    for i in range(21):
        m.append([0] * 21)

    for a in agents:
        #print a.loc
        m[a.loc[0] + 9][a.loc[1] + 9] += 1 

    for l in m:
        print ''.join(str(x) if x > 0 else '-' for x in l)
    print '=' * 20


def run(N=10, iterations=100, distance='inverse_euclidean'):
    agents = [Agent() for i in range(N)]

    print_map(agents, 'BEGIN')

    for i in range(iterations):
        for a in agents:
            for b in agents:
                if a is b:
                    continue
                b.accept(Message(a.loc, 'signal', a.pdist, None))
        for a in agents:
            a.move()
    print_map(agents, 'END {}'.format(i))



if __name__ == '__main__':
    run()
