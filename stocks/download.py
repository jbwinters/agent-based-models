import sys

import joblib
import pandas as pd
import quandl

quandl.ApiConfig.api_key = 'us3MHK9zsuHUCCz_EBP4'

filename = sys.argv[1]
djia_df = pd.read_csv(filename)

df = quandl.get(list(djia_df.free_code), returns='pandas', start_date='2015-05-19')

joblib.dump(df, filename.split('.')[0], compress=9)
