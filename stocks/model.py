from collections import defaultdict
import sys

import joblib
import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import pairwise_distances as pdist

df = joblib.load('djia' if len(sys.argv) != 2 else sys.argv[1])

close_cols = [x for x in df.columns if '- Close' in x]
volume_cols = [x for x in df.columns if '- Volume' in x]

df = df.T

dfs = []
for c, v in zip(close_cols, volume_cols):
    code = c.split(' - ')[0].replace('WIKI/', '')
    dfs.append(pd.DataFrame({code: df.loc[c] * df.loc[v]},
        index=df.loc[c].index))

mdf = pd.concat(dfs, axis=1, join='inner')
mdf = mdf.dropna()

deltas = []

for i in range(1, len(mdf)):
    deltas.append((mdf.iloc[i] - mdf.iloc[i - 1]) / (1 + mdf.iloc[i - 1]))

pts = 100 * np.random.rand(len(mdf.columns), len(mdf.columns))

pt_deltas = []

for d in deltas:
    for i, x in enumerate(d):
        for j, y in enumerate(d):
            pt_deltas.append(np.array(pts))
            mult = 0.5 if np.sign(x) == np.sign(y) else 2.0
            pts[i][j] = pts[i][j] * mult
            pts[j][i] = pts[j][i] * mult

codes = [c.split(' - ')[0].replace('WIKI/', '') for c in close_cols]
pts_df = pd.DataFrame(pts)

dists = pd.DataFrame(pdist(pts_df))
dists.columns = codes
dists.index = codes

def prdist(key):
    x = dists[[key]].sort_values([key])
    print x

bs = defaultdict(list)
for c in dists.columns:
    v = dists[[c]].sort_values([c])
    for i, b in zip(range(len(v)), v.index):
        bs[b].append(i)
    ranks = zip(range(len(v)), v.index)

dfs = []
for k in sorted(bs):
    dfs.append(pd.DataFrame({'code': [k], 'mean_rank': np.mean(bs[k]), 'std': np.std(np.array(bs[k]))}))

ranks = pd.concat(dfs)

print ranks.sort_values(['mean_rank'])

import pdb; pdb.set_trace()

