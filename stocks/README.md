This model imagines stock market as a network in high-dimensional space where volatility of important stocks has strong influence on the volatility less important nearby stocks.
It begins by assigning every stock a random location in an N-dimensional space, where N is the number of stocks being sampled. When stock `i` and stock `j` move in the same direction (up or down), the value of stock `j`'s location vector at position `i` is cut in half, bringing them closer together.
When stock `i` and stock `j` move in opposite directions, the value is multiplied by 2, pushing them further apart.

In effect this jiggles the respective firms around until they're arranged to represent the structure of the network. 
That's the idea at least - the intention behind this model is to divine the structure of a stock market. 
I'm not sure if it adequately captures delayed effects though or if the spatial representation does what I want it to, both which might break the whole idea... needs more thought.

This is a sketch so I haven't really delved into the details to see if the code is behaving the way I want. In the limited testing I have done, oil companies and banks seemed to make their way to the center of the universe, which isn't too surprising but needs to be verified with further study.

Stock data is from Quandl.
